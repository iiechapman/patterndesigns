//
//  SDLApp.h
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/28/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#ifndef __GameEngineTests1__SDLApp__
#define __GameEngineTests1__SDLApp__

#include <iostream>
#include <SDL2/SDL.h>

#define APPTITLE "SDL Template Program"
// SdlApplication is nothing more than thin wrapper to SDL library. You need
// just to instantiate it and call run() to enter the SDL event loop.
class SDLApp
{
public:
	SDLApp();
	~SDLApp();
	
	// Application state (just convenience instead of 0, 1, ...).
	enum APP_STATE
	{
		APP_OK = 0,
		APP_FAILED = 1
	};
	
	// Initialize application, called by run(), don't call manually.
	int init(int width, int height);
	
	// Destroy application, called by destructor, don't call manually.
	void destroy();
	
	// Run application, called by your code.
	int run(int width, int height);
	
	// Called to process SDL event.
	void onEvent(SDL_Event* ev);
	
	// Called to render content into buffer.
	void Render();
	
private:
	// Whether the application is in event loop.
	bool running;
	SDL_Window *window;
	SDL_Renderer *renderer;
};




#endif /* defined(__GameEngineTests1__SDLApp__) */
