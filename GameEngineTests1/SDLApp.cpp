//
//  SDLApp.cpp
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/28/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#include "SDLApp.h"

//Creates App
SDLApp::SDLApp():running(false)
{
    
}

//Destroys app
SDLApp::~SDLApp()
{
	destroy();
}

int SDLApp::init(int width, int height)
{
	// Initialize the SDL library.
	if (SDL_Init(SDL_INIT_VIDEO | SDL_INIT_TIMER) < 0)
	{
		fprintf(stderr, "SDL_Init() failed: %s\n", SDL_GetError());
		return APP_FAILED;
	}
	
	window = SDL_CreateWindow(
                              APPTITLE,
                              SDL_WINDOWPOS_CENTERED,
                              SDL_WINDOWPOS_CENTERED,
                              width, height, SDL_WINDOW_SHOWN);
    
	renderer = SDL_CreateRenderer(
                                  window,
                                  -1,
                                  SDL_RENDERER_ACCELERATED | SDL_RENDERER_PRESENTVSYNC
                                  );
	
	// Success.
	return APP_OK;
}

void SDLApp::destroy()
{
	if (window)
	{
		SDL_DestroyWindow(window);
		SDL_DestroyRenderer(renderer);
		SDL_Quit();
	}
}

int SDLApp::run(int width, int height)
{
	// Initialize application.
	int state = init(width, height);
	if (state != APP_OK) return state;
	
	// Enter to the SDL event loop.
	SDL_Event ev;
	running = true;
	
	while (SDL_WaitEvent(&ev))
	{
		onEvent(&ev);
		Render();
		
		if (running == false)
		{
			break;
		}
	}
	
	return APP_OK;
}

void SDLApp::onEvent(SDL_Event* ev)
{
	switch (ev->type)
	{
		case SDL_QUIT:
			running = false;
			break;
			
		case SDL_KEYDOWN:
		{
			if (ev->key.keysym.sym == SDLK_ESCAPE)
			{
				running = false;
			}
		}
	}
}

void SDLApp::Render()
{
}





























