//
//  GameApp.cpp
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/30/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#include "GameApp.h"


void GameApp::onEvent(SDL_Event* ev)
{
    App::onEvent(ev);
}


void GameApp::Render()
{
    App::Render();
}



int GameApp::init(int width, int height)
{
    App::init(width,height);
    
    
    //No errors
    return APP_OK;
}


void GameApp::destroy()
{
    App::destroy();
}


int GameApp::run(int width,int height)
{
    App::run(width,height);
    
    
    //No Errors
    return APP_OK;
}

