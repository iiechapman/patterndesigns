//
//  App.cpp
//  GameEngineTests1
//
//  Created by Evan Chapman on 12/30/13.
//  Copyright (c) 2013 Evan Chapman. All rights reserved.
//

#include "App.h"
int App::init(int width, int height)
{
    SDLApp::init(width,height);
    return APP_OK;
}


void App::destroy()
{
    SDLApp::destroy();
}


int App::run(int width,int height)
{
    SDLApp::run(width,height);
    return APP_OK;
}


void App::onEvent(SDL_Event* ev)
{
    SDLApp::onEvent(ev);
}


void App::Render()
{
    SDLApp::Render();
}
